<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p>
        <h2>Hallo world</h2>
    </p>

    <?php
        $kalimat1= "latihan PHP untuk pemula";
        echo "kalimat: " .$kalimat1 . "<br>";
        echo "jumlah karakter kalimat pertama:  ". strlen($kalimat1)."<br>";
        echo "jumlah kata:  " .str_word_count($kalimat1)."<br> <br>";

        echo "<h3>contoh 2</h3>";
        $string2= "hello world";
        echo "kalimat kedua     :" . $string2 . "<br>";
        echo "kata pertama      :" . substr($string2, 0,5) . "<br>";
        echo "kata pertama      :" . substr($string2, 6,11);
    
    
        echo "<h3> Soal No 1</h3>";
       
        /* 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat 
tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        */

        $first_sentence = "Hello PHP!" ; // Panjang string 10, jumlah kata: 2
        echo "Panjang string    :". strlen($first_sentence). "<br>";
        echo "jumlah kata       :".str_word_count($first_sentence). "<br>";
        $second_sentence = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5
        echo "Panjang string    :". strlen($second_sentence). "<br>";
        echo "jumlah kata       :".str_word_count($second_sentence). "<br>";



        echo "<h3> Soal No 2</h3>";
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: " . substr($string2,2,5);
        echo "<br> Kata Ketiga: " . substr($string2,6,10);

        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but Good!";
        echo "String: \"$string3\" "; 
        // OUTPUT : "PHP is old but awesome"
        echo "<br>"."ganti kalimat soal no. 3       :" .str_replace("good", "Awesome", $string3);


    ?>
</body>
</html>